package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.*;

class UserDAOTest {

    @Test
    void changeTest1() {
        User user = new User("nickname", "mail", null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }

    @Test
    void changeTest2() {
        User user = new User("nickname", "mail", null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    void changeTest3() {
        User user = new User("nickname", "mail", "fullname", null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("fullname"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    void changeTest4() {
        User user = new User("nickname", "mail", null, "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    void changeTest5() {
        User user = new User("nickname", null, "fullname", "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("fullname"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }

    @Test
    void changeTest6() {
        User user = new User("nickname", "mail", "fullname", "about");
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new UserDAO(mockJdbc);
        UserDAO.Change(user);
        verify(mockJdbc).update(
                Mockito.eq("UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
                Mockito.eq("mail"),
                Mockito.eq("fullname"),
                Mockito.eq("about"),
                Mockito.eq(user.getNickname()));
    }
}
