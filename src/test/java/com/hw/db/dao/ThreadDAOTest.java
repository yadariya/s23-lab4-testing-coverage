package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDAOTest {
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ThreadDAO thread = new ThreadDAO(mockJdbc);
    @Test
    void test1() {
        ThreadDAO.treeSort(1, 2, 3, null);
        verify(mockJdbc)
                .query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  " +
                                "AND branch > (SELECT branch  FROM posts WHERE id = ?)  " +
                                "ORDER BY branch LIMIT ? ;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.eq(1),
                        Mockito.eq(3),
                        Mockito.eq(2));
    }

    @Test
    void test2() {
        ThreadDAO.treeSort(1, 2, 3, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  " +
                        "AND branch < (SELECT branch  FROM posts WHERE id = ?)  " +
                        "ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(1),
                Mockito.eq(3),
                Mockito.eq(2));
    }
}
