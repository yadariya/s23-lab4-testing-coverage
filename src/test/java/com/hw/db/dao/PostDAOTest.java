package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;

import static org.mockito.Mockito.*;

class PostDAOTest {
    @Test
    void test1() {
        Post post = new Post("author", new Timestamp(0), null, "message", null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setAuthor("old_author");
        prev.setMessage("old_message");
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt())).thenReturn(prev);
        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);
        verify(mockJdbc)
                .update(Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                        Mockito.eq("author"), Mockito.eq("message"),
                        Mockito.eq(new Timestamp(0)),
                        Mockito.eq(0));
    }

    @Test
    void test2() {
        Post post = new Post(null, new Timestamp(0), null, "message", null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setMessage("old_message");
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(Mockito.any(String.class),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt())).thenReturn(prev);
        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);
        verify(mockJdbc)
                .update(Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                        Mockito.eq("message"),
                        Mockito.eq(new Timestamp(0)),
                        Mockito.eq(0));
    }

    @Test
    void test3() {
        Post post = new Post(null, new Timestamp(0), null, null, null, null, null);
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Post prev = new Post();
        prev.setCreated(new Timestamp(1));
        Mockito.when(mockJdbc.queryForObject(
                Mockito.any(String.class),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt())).thenReturn(prev);
        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);
        verify(mockJdbc)
                .update(Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                        Mockito.eq(new Timestamp(0)),
                        Mockito.eq(0));
    }

    @Test
    void test4() {
        Post post = new Post();
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        Mockito.when(mockJdbc.queryForObject(
                Mockito.any(String.class),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt())).thenReturn(post);
        new PostDAO(mockJdbc);
        PostDAO.setPost(0, post);
        verify(mockJdbc, never()).update(Mockito.any(String.class), Mockito.any(Object[].class));
    }
}
